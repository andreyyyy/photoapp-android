package com.andrey.photoapp;
//Andrey Ilkiv and Brian Lemes

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    recyclerAdapter adapter;

    EditText text;
    Button newAlbum;
    String text1;

    ArrayList<String> namelist = new ArrayList<String>();
    SQLiteDatabase database;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        text = (EditText) findViewById(R.id.textView4);
        newAlbum = (Button)findViewById(R.id.button7);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        adapter = new recyclerAdapter(MainActivity.this);
        recyclerView.setLayoutManager(new GridLayoutManager(MainActivity.this,1));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        database = openOrCreateDatabase("gallery",MODE_PRIVATE,null);
        database.execSQL("CREATE TABLE IF NOT EXISTS meriGallery(ANames)");
        loadData();
//        namelist.add("camera");
//        namelist.add("new");

        //text1 = text.getText().toString();

        newAlbum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addAlbum();
            }
        });

    }

    public void loadData()
    {
        namelist.clear();
        Cursor s = database.rawQuery("SELECT * FROM meriGallery",null);
        int anameIndex = s.getColumnIndex("ANames");
        Toast.makeText(this, "Hello"+s.moveToFirst(), Toast.LENGTH_SHORT).show();
        if(s.moveToFirst()) {
            Log.d("tryAlbum",s.getString(anameIndex));
            namelist.add(s.getString(anameIndex));
            while (s.moveToNext())
            {
                namelist.add(s.getString(anameIndex));
            }
            adapter.notifyDataSetChanged();
        }
    }
    public void addAlbum()
    {
        //namelist.add(text1);

           // PrintWriter writer = new PrintWriter(new FileOutputStream(new File(text.getText().toString())));
            database.execSQL("INSERT INTO meriGallery(ANames) VALUES('"+text.getText().toString()+"')");
            database.execSQL("CREATE TABLE IF NOT EXISTS "+text.getText().toString()+" (uri,location,person)");
            loadData();
            // finish();
        //startActivity(getIntent());
    }




    public class recyclerAdapter extends RecyclerView.Adapter<recyclerAdapter.viewHolder>
    {

        private Context context;

        private ArrayList<String> names = new ArrayList<String>();

        public recyclerAdapter(Context context)
        {
            this.context = context;


        }

        @Override
        public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View vie = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_layout,null);
            return new viewHolder(vie);
        }

        @Override
        public void onBindViewHolder(final viewHolder holder, final int position) {

            //holder.setListeners();
            holder.imgbutton.setText(namelist.get(position));
//            holder.imgbutton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    namelist.remove(position);
//                    notifyDataSetChanged();
//
//                }
//            });


            holder.imgbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("tryGal","pressed");
                    Intent intent = new Intent(MainActivity.this,Album.class);
                    intent.putExtra("tname",namelist.get(position));
                    startActivity(intent);

                }
            });

            holder.addBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //onButtonShowPopupWindowClick(view);
                    String name = holder.name.getText().toString();
                    //namelist.add(name);

                    holder.imgbutton.setText(name);
//                    database.execSQL("UPDATE meriGallery(ANames) SET ANames='"+name+"' WHERE ANames='"+namelist.get(position)+"'");
                    ContentValues cv = new ContentValues();
                    cv.put("ANames",name);
                    database.update("meriGallery",cv,"ANames='"+namelist.get(position)+"'",null);
                    database.execSQL("ALTER TABLE " + namelist.get(position) + " RENAME TO " + holder.name.getText().toString());
                    holder.imgbutton.setText(holder.name.getText().toString());
                    namelist.set(position, name);



                    //notifyItemInserted(namelist.size());
                    //loadData();
                    //notifyDataSetChanged();

                }
            });
            holder.removeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        database.execSQL("DELETE FROM meriGallery WHERE ANames='"+namelist.get(position)+"'");
                        database.execSQL("DROP TABLE IF EXISTS "+namelist.get(position));
                    } catch (SQLException e) {
                        Log.d("tryError",e.getMessage());
                        e.printStackTrace();
                    }
                    namelist.remove(position);
                    notifyDataSetChanged();


                }
            });
        }

        @Override
        public int getItemCount() {
            return namelist.size();
        }

        public class viewHolder extends RecyclerView.ViewHolder
        {
            Button imgbutton,addBtn,removeBtn;
            EditText name;


            public viewHolder(View itemView) {
                super(itemView);

                imgbutton = (Button)itemView.findViewById(R.id.button);
                addBtn = (Button)itemView.findViewById(R.id.button5);
                removeBtn = (Button)itemView.findViewById(R.id.button6);

                name = (EditText) itemView.findViewById(R.id.textView);

            }

        }
    }
/*
    public void onButtonShowPopupWindowClick(View view) {

        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.pop_up_window, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }*/
}
