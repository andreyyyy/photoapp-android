package com.andrey.photoapp;
//Andrey Ilkiv and Brian Lemes

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class searchActivity extends AppCompatActivity {

    ImageView imgView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        imgView = (ImageView)findViewById(R.id.imageView2);
        String data = getIntent().getExtras().getString("img");
        imgView.setImageURI(Uri.parse(data));
    }
}
