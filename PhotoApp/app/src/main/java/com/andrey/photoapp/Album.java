package com.andrey.photoapp;
//Andrey Ilkiv and Brian Lemes

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class Album extends AppCompatActivity {
    String tableName = "";
    ArrayList<String> imagesList = new ArrayList<>();
    ArrayList<String> personList = new ArrayList<>();
    ArrayList<String> locationList = new ArrayList<>();


    SQLiteDatabase database;
    recyclerAdapter adapter;
    Button newAlbum;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        newAlbum = (Button)findViewById(R.id.button7);
        tableName = getIntent().getExtras().getString("tname");
        Log.d("tryGal",tableName);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        adapter = new recyclerAdapter(this);
        recyclerView.setLayoutManager(new GridLayoutManager(this,1));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        database = openOrCreateDatabase("gallery",MODE_PRIVATE,null);
        loadData();
       // database.execSQL("CREATE TABLE IF NOT EXISTS "+tableName+"(uri,location,person)");

        newAlbum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                addAlbum();
            }
        });


        final EditText bar = (EditText)findViewById(R.id.editText3);
        Button search = (Button)findViewById(R.id.button8);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchData(bar.getText().toString());
            }
        });

    }

    public void addAlbum()
    {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1234);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == 1234)
        {
            Log.d("tryGal",data.getData().toString());
            database.execSQL("INSERT INTO "+tableName+"(uri,location,person) VALUES('"+data.getData().toString()+"','','')");
            loadData();
        }
    }


    public void searchData(String text)
    {

        Cursor s = null;//database.rawQuery("SELECT * from "+tableName+" where person like'%"+text+"''%s'",null);
        String sql = "SELECT * FROM " + tableName +
                " WHERE person  LIKE ? OR location LIKE ?";
        s = database.rawQuery(sql, new String[]{"%" +text + "%","%" +text + "%"});
        int anameIndex = s.getColumnIndex("uri");
        int pnameIndex = s.getColumnIndex("person");
        int lnameIndex = s.getColumnIndex("location");
        imagesList.clear();
        personList.clear();
        locationList.clear();
        if(s.moveToFirst()) {
            imagesList.add(s.getString(anameIndex));
            personList.add(s.getString(pnameIndex));
            locationList.add(s.getString(lnameIndex));
            adapter.notifyDataSetChanged();
            while (s.moveToNext())
            {
                imagesList.add(s.getString(anameIndex));
                personList.add(s.getString(pnameIndex));
                locationList.add(s.getString(lnameIndex));
                adapter.notifyDataSetChanged();
            }
            adapter.notifyDataSetChanged();
        }
        adapter.notifyDataSetChanged();
    }

    public void loadData()
    {
        Cursor s = database.rawQuery("SELECT * FROM "+tableName,null);
        int anameIndex = s.getColumnIndex("uri");
        int pnameIndex = s.getColumnIndex("person");
        int lnameIndex = s.getColumnIndex("location");
        imagesList.clear();
        if(s.moveToFirst()) {
            imagesList.add(s.getString(anameIndex));
            personList.add(s.getString(pnameIndex));
            locationList.add(s.getString(lnameIndex));
            adapter.notifyDataSetChanged();
            while (s.moveToNext())
            {
                imagesList.add(s.getString(anameIndex));
                personList.add(s.getString(pnameIndex));
                locationList.add(s.getString(lnameIndex));
                adapter.notifyDataSetChanged();
            }
            adapter.notifyDataSetChanged();
        }
    }

    public class recyclerAdapter extends RecyclerView.Adapter<recyclerAdapter.viewHolder>
    {

        private Context context;
        public recyclerAdapter(Context context)
        {
            this.context = context;
        }

        @Override
        public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View vie = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_for_image,null);
            return new viewHolder(vie);
        }

        @Override
        public void onBindViewHolder(final viewHolder holder, final int position) {

            holder.imageT.setImageURI(Uri.parse(imagesList.get(position)));
            holder.location.setText(locationList.get(position));
            holder.person.setText(personList.get(position));
            holder.personEB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.person.setText(holder.personE.getText().toString());
                    String where = "uri=?";
                    String[] whereArgs = new String[] {imagesList.get(position)};
                    ContentValues dataToInsert = new ContentValues();
                    dataToInsert.put("person", holder.person.getText().toString());
                    database.update(tableName, dataToInsert, where, whereArgs);
                   // database.execSQL("UPDATE "+tableName+"(uri,location,person) SET person = '"+holder.personE.getText().toString()+"'");
                }
            });

            holder.locationEB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.location.setText(holder.locationE.getText().toString());
                    String where = "uri=?";
                    String[] whereArgs = new String[] {imagesList.get(position)};
                    ContentValues dataToInsert = new ContentValues();
                    dataToInsert.put("location", holder.locationE.getText().toString());
                    database.update(tableName, dataToInsert, where, whereArgs);
                }
            });

            holder.imageT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Album.this, searchActivity.class);
                    intent.putExtra("img",imagesList.get(position).toString());
                    startActivity(intent);
                }
            });

            holder.del.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                     database.execSQL("DELETE FROM "+tableName+" WHERE uri='"+imagesList.get(position)+"'");
                     imagesList.remove(position);
                     notifyDataSetChanged();
                }
            });
//            //holder.setListeners();
//            holder.imgbutton.setText(imagesList.get(position));
//
//
//            holder.imgbutton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if(position == 0) {
//
//                    }
//
//                }
//            });
//
//            holder.addBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    //onButtonShowPopupWindowClick(view);
//                    String name = holder.name.getText().toString();
//                    //namelist.add(name);
//                    imagesList.set(position, name);
//                    holder.imgbutton.setText(name);
//                    notifyDataSetChanged();
//
//
//                }
//            });
//            holder.removeBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    imagesList.remove(position);
//                    notifyDataSetChanged();
//                }
//            });
        }

        @Override
        public int getItemCount() {
            return imagesList.size();
        }

        public class viewHolder extends RecyclerView.ViewHolder
        {
//            Button imgbutton,addBtn,removeBtn;
//            EditText name;
            ImageView imageT;
            TextView person;
            TextView location;
            EditText personE;
            EditText locationE;
            Button personEB;
            Button locationEB;
            Button del;

            public viewHolder(View itemView) {
                super(itemView);

                imageT = (ImageView)itemView.findViewById(R.id.imageView);
                person = (TextView)itemView.findViewById(R.id.textView2);
                location = (TextView)itemView.findViewById(R.id.textView3);
                personE = (EditText) itemView.findViewById(R.id.editText);
                locationE = (EditText) itemView.findViewById(R.id.editText2);
                personEB = (Button) itemView.findViewById(R.id.button2);
                locationEB = (Button) itemView.findViewById(R.id.button3);
                del = (Button) itemView.findViewById(R.id.button4);
//                imgbutton = (Button)itemView.findViewById(R.id.button);
//                addBtn = (Button)itemView.findViewById(R.id.button5);
//                removeBtn = (Button)itemView.findViewById(R.id.button6);
//
//                name = (EditText) itemView.findViewById(R.id.textView);

            }

        }
    }
}
